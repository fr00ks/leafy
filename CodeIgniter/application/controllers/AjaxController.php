<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AjaxController extends CI_Controller{

  protected $CurrentUser;
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

  }

  public function LoginAjax()
  {
    $CurrentUser = $this->session->userdata('current_user');
    if (!empty($this->current_user))
    {
      base_url();
    }

    $Data = array();
    $Data['FormData'] = $this->input->post();

    $this->form_validation->set_rules('email','email','trim|required');
    $this->form_validation->set_rules('password', 'password','trim|required');

    if ($this->form_validation->run() == TRUE)//Si pasan las validaciones
    {
      $this->load->model->('UserModel');
      $Result = $this->UserModel->LogUser($Data['FormData']);
      if (!is_string($Result))//TodoOk
      {
        echo json_encode(array('success' => 1, 'data' => '','msg'=> ''));
      }
      else//Enviamos el $Result si no va bien
      {
        echo json_encode(array('success' => 0,'data' => array('user' ,'password'),
        'msg' => $Result));
      }
    }
    else
    {
      foreach ($this->form_validation->error_array() as $key => $value)
      {
        $ErrorFields[] = $key;
      }
      echo json_encode(array('success' => 0, 'data' => $ErrorFields, 'msg' =>"<b style='color: yellow;'>Formulario Incompleto</b><br>Por favor, revise los campos marcados y vuelva a intentarlo."))
    }

  }

  public function SignupAjax()
  {
    //Si tenemos post
    if ($this->input->post() != "")
    {
      $Data = array();
      $Data['FormData'] = $this->input->post();

      $ErrorFields = array();
      $ErrorMsg = array();

      //Validaciones del formulario de registro
      $this->form_validation->set_rules('name', 'name', 'required');
      $this->form_validation->set_rules('email','email','required|trim|is_unique[User.email]');
      $this->form_validation->set_rules('password', 'password', 'required|trim');
      $this->form_validation->set_rules('DOB', 'DOB', 'required');
      $this->form_validation->set_rules('mobile', 'mobile', 'required|numeric|min_length[9]');

      //Mensajes de las validaciones
      $this->form_validation->set_message('required', '<b style="color: yellow;">REQUIRED:Formulario Incompleto</b><br> <b>Por favor, revisa los campos marcados y vuelve a intentarlo.</b>');
      $this->form_validation->set_message('is_unique', '<b style="color: yellow;">IS_UNIQUE:Formulario Incompleto</b><br> <b>Por favor, revisa los campos marcados y vuelve a intentarlo.</b>');
      $this->form_validation->set_message('min_length', '<b style="color: yellow;">MINLEN:Formulario Incompleto</b><br> <b>Por favor, revisa los campos marcados y vuelve a intentarlo.</b>');
      $this->form_validation->set_message('trim', '<b style="color: yellow;">TRIM:Formulario Incompleto</b><br> <b>Por favor, revisa los campos marcados y vuelve a intentarlo.</b>');
      $this->form_validation->set_message('numeric', '<b style="color: yellow;">numeric:Formulario Incompleto</b><br> <b>Por favor, revisa los campos marcados y vuelve a intentarlo.</b>');

      if($this->form_validation->run() == TRUE)//Si pasa las validaciones
      {

         $Result = $this->UserModel->CreateUser($Data['FormData']);
         if ($Result == TRUE)//Si lo que devuelve es insertado
         {
           echo json_encode(array('success' => 1, 'data' => '', 'msg' =>'' ));
         }
         else
   			 {
   			    foreach($this->form_validation->error_array() as $key => $value)
   			    {
   			        $ErrorFields[] = $key;
   			        $ErrorMsg[] = $value;
   			    }
   			    echo json_encode(array('success' => 0, 'data' => $ErrorFields, 'msg' => $ErrorMsg[0]));
   			 }
      }
    }
    else
    {
      foreach ($this->form_validation->error_array() as $key => $value)
      {
        $ErrorFields[] = $key;
        $Error
      }
    }
  }
}
