<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class index extends CI_Controller{

  public function index()
  {

    $data['todo_list'] = array('Clean House', 'Call Mom', 'Run Errands');

    $data['title'] = "LeafyERP";
    $data['heading'] = "The real Thing";

    $this->load->view('IndexView', $data);

  }


}
?>
